package com.pawelbanasik;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class BankAccountTest {
	private BankAccount account;
	private static int count;

	// wykonuje sie raz przed testami
	@BeforeClass
	public static void beforeClass() {
		System.out.println("This executes before any test cases. Count = " + count++);
	}

	// metoda z anotacja Before wykonuje sie przed kazda testowana metoda
	// przed kazdym testem sie wykona czyli tu kilka razy
	@Before
	public void setup() {
		account = new BankAccount("Tim", "Buchalka", 1000, BankAccount.CHECKING);
		System.out.println("Running a test...");
	}

	@Test
	public void deposit() {
		// to juz nie jest potrzebne poniewaz mamy zrobiona metode before
		// BankAccount account = new BankAccount("Tim", "Buchalka", 1000,
		// BankAccount.CHECKING);
		double balance = account.deposit(200, true);
		assertEquals(1200, balance, 0);
		assertEquals(1200, account.getBalance(), 0);
	}

	@Test
	public void withdraw_branch() throws Exception {
		double balance = account.withdraw(600, true);
		assertEquals(400, balance, 0);
	}

	@Test (expected = IllegalArgumentException.class)
	public void withdraw_notBranch() throws Exception {
		double balance = account.withdraw(600, false);
		assertEquals(400, balance, 0);
	}

	@Test
	public void getBalance_deposit() {
		// BankAccount account = new BankAccount("Tim", "Buchalka", 1000,
		// BankAccount.CHECKING);
		account.deposit(200, true);
		assertEquals(1200, account.getBalance(), 0);

	}

	@Test
	public void getBalance_witdraw() {
		// BankAccount account = new BankAccount("Tim", "Buchalka", 1000,
		// BankAccount.CHECKING);
		account.withdraw(200, true);
		assertEquals(800, account.getBalance(), 0);

	}

	@Test
	public void isChecking_true() {
		// BankAccount account = new BankAccount("Tim", "Buchalka", 1000,
		// BankAccount.CHECKING);
		assertTrue(account.isChecking());
	}

	@AfterClass
	public static void afterClass() {
		System.out.println("This executes after any test cases. Count = " + count++);

	}

	@After
	public void teardown() {
		System.out.println("Count = " + count++);
	}
}
